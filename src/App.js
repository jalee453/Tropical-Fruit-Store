import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

// styles
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

// components
import Fruit from './components/Fruit';
import Checkout from './components/Checkout';

// tropical fruits
import tropicalFruits from './tropical-fruits.json';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cart: [],
      cartCount: 0,
      total: 0.0,
      personName: '',
      isDisabled: true,
    };
  }

  checkId(cart, id) {
    return cart.some((item) => {
      return item.id === id;
    })
  }

  addToCart(item) {
    const cart = this.state.cart.slice();
    let price = 0.0;

    if (this.checkId(cart, item.id)) {
      let index = cart.findIndex((x => x.id === item.id));
      price = cart[index].price;
      cart[index].quantity += 1;
    } else {
      cart.push(item);
      let index = cart.findIndex((x => x.id === item.id));
      price = cart[index].price;
      cart[index].quantity += 1;
    }

    this.setState((prevState) => ({
      cart: cart,
      cartCount: prevState.cartCount + 1,
      total: prevState.total + price
    }));
  }

  handleChange(e) {
    if (e.target.value !== '')
      this.setState({ personName: e.target.value, isDisabled: false });
    else
      this.setState({ personName: '', isDisabled: true })
  }

  render() {
    return (
      <Router>
        <div className="container-fluid">
          <nav className="navbar navbar-dark" style={{ 'backgroundColor': '#0C9969' }}>
            <Link to={'/'}>
              <span className="h1 navbar-brand mb-0">Tropical Fruit Store</span>
            </Link>
          </nav>
          <Route exact={true} path="/" render={() => (
            <div>
              <div>
                <Link to={'/checkout'}>
                  <button className="btn btn-dark btn-checkout">
                    Go to Checkout <span className="badge badge-secondary">{this.state.cartCount}</span>
                  </button>
                </Link>
              </div>
              {tropicalFruits.map((fruit) => {
                return (
                  <Fruit
                    key={fruit.id}
                    image={fruit.image}
                    name={fruit.name}
                    price={fruit.price}
                    addToCart={() => this.addToCart(fruit)}
                  />
                );
              })}
            </div>
          )} />
          <Route path="/checkout" render={() =>
            <Checkout
              cart={this.state.cart}
              total={this.state.total}
              personName={this.state.personName}
              isDisabled={this.state.isDisabled}
              handleChange={(event) => this.handleChange(event)}
            />}
          />
          <Route path="/confirmation" render={() => <ThankYou personName={this.state.personName} />} />
        </div>
      </Router>
    );
  }
}

const ThankYou = (props) => {
  return (
    <div class="h3 alert alert-success" role="alert">
      Thank you {props.personName}! Your order was successfully submitted.
    </div>
  );
}

export default App;
