import React from 'react';
import { Link } from 'react-router-dom';
import './Fruit.css';

const Checkout = (props) => {
    return (
        <div>
            {props.cart.map((item) => {
                return (
                    <div key="item.id" className="card fruit">
                        <img className="card-img-top" src={item.image} alt={item.name} />
                        <div className="card-body">
                            <p className="card-text caption">Quantity: {item.quantity}</p>
                        </div>
                    </div>
                );
            })}
            <p>Total: ${props.total.toFixed(2)}</p>
            <div className="row">
                <div className="col-lg-2">
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="Name" aria-label="Name" onChange={(event) => props.handleChange(event)} value={props.personName} />
                    </div>
                </div>
            </div>
            <Link to={'/confirmation'}>
                <button type="button" className="btn btn-primary btn-checkout" disabled={props.isDisabled || props.subtotal === 0 ? "disabled" : false}>
                    Order Fruit Now
                </button>
            </Link>
        </div>
    );
}

export default Checkout;